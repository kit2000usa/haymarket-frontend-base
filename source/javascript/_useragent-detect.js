
ready(function() {
	var ad = new UserAgentDetect();		
	var htmlElement = document.getElementsByTagName("html")[0];
	if (htmlElement.classList)  htmlElement.classList.add(ad.device(), ad.os(), ad.browser());
	else htmlElement.className += ' ' + ad.device() + ' ' + ad.os() + ' ' + ad.browser();
});


/***************************************************/
/******************* Desktop Browser Detect ********/
/***************************************************/

function DesktopBrowserDetech() {
	if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0)  return "Opera";
	if (typeof InstallTrigger !== 'undefined')  return "Firefox";
	if (Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0) return "Safari";
	if (!!window.chrome && !!window.chrome.webstore) return "Chrome";
	if (navigator.userAgent.indexOf('MSIE 6.0') >= 0) return "IE6";
	if (navigator.userAgent.indexOf('MSIE 7.0') >= 0) return "IE7";
	if (navigator.userAgent.indexOf('MSIE 8.0') >= 0) return "IE8";
	if (navigator.userAgent.indexOf('MSIE 9.0') >= 0) return "IE9";
	if (navigator.userAgent.indexOf('MSIE 10.0') >= 0) return "IE10";
	if (navigator.userAgent.indexOf('rv:11.0') >= 0) return "IE11";
	if (navigator.userAgent.indexOf('Edge') >= 0) return "Edge";
}

/***************************************************/
/******************* User Agent Detect *************/
/***************************************************/

function UserAgentDetect() {
	
	var md = new MobileDetect(window.navigator.userAgent);

	// check device or desktop
	if (md.phone() != null) {
		this.device =  function() {return "Mobile"};
		this.os = function() {return md.os()}; // Output: AndroidOS, BlackBerryOS, PalmOS, SymbianOS, WindowsMobileOS, WindowsPhoneOS, iOS, MeeGoOS, MaemoOS, JavaOS, webOS, badaOS, BREWOS
		this.browser = function() {return md.userAgent()}; // Output: Chrome, Dolfin, Opera, Skyfire, IE, Firefox, Bolt, TeaShark, Blazer, Safari, Tizen, UCBrowser, baiduboxapp, baidubrowser, DiigoBrowser, Puffin, Mercury, ObigoBrowser, NetFront, GenericBrowser
	}
	if (md.tablet() != null) {
		this.device =  function() {return "Tablet"};
		this.os = function() {return md.os()}; 
		this.browser = function() {return md.userAgent()};
	}
	if (md.mobile() == null) {
		this.device =  function() {return "Desktop"};
		this.os = function() {return navigator.platform}; // Output: Win32, Linux i686, MacPPC, MacIntel
		this.browser = function() {return DesktopBrowserDetech()}; // Output: Opera, Firefox, Safari, Chrome, IE6-IE11+
	}

	/*
	console.log("device = " + this.device());
	console.log("os = " + this.os());
	console.log("browser = " + this.browser());
	*/
}

/***************************************************/
/******************* Document Ready ****************/
/***************************************************/

function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}
