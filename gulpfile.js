/* globals require, exports */

"use strict";

var gulp 		  = require("gulp"),
	uglify 		  = require("gulp-uglify"),
	concat 		  = require("gulp-concat"),
	sass       	  = require("gulp-sass"),
    sourcemaps 	  = require("gulp-sourcemaps"),
	cleanCSS 	  = require("gulp-clean-css"),
	rename 		  = require("gulp-rename"),

  js_input  = {
  	"javascript": "./source/javascript/**/*.js",
  	"slick_carousel": "./bower_components/slick-carousel/slick/slick.js",
  	"magnific_popup": "./bower_components/magnific-popup/dist/*.js",
  	"flexslider": "./bower_components/flexslider/jquery.flexslider.js",
  	"modernizr": "./bower_components/modernizr/modernizr.js",
  	"jquery": "./bower_components/jquery/dist/jquery.js",
  },

  css_input ={
  	"stylesheet": "./source/scss/**/*.scss",
  	"fontawesome": "./bower_components/font-awesome/css/font-awesome.min.css",
  	"slick_carousel_css": "./bower_components/slick-carousel/slick/*.css",
  	"flexslider_css": "./bower_components/flexslider/flexslider.css",
  	"magnific_popup_css": "./bower_components/magnific-popup/dist/*.css",
  },

  output = {
  	"stylesheets": "./public/assets/stylesheets",
  	"javascript": "./public/assets/javascript"
  };


gulp.task("build-css", function() {
  return gulp.src(css_input.stylesheet)
	.pipe(sourcemaps.init())  // Process the original sources
  .pipe(sass())
  .pipe(gulp.dest(output.stylesheets))
  .pipe(cleanCSS())
  .pipe(rename("style.min.css"))
	//.pipe(sourcemaps.write()) // Add the map to modified source.
	.pipe(gulp.dest(output.stylesheets));
});

gulp.task("vendor-css", function() {
	return gulp.src([
		css_input.fontawesome, 
		css_input.slick_carousel_css, 
		css_input.flexslider_css, 
		css_input.magnific_popup_css 
	])
	.pipe(sourcemaps.init())  // Process the original sources
	.pipe(cleanCSS())
	//.pipe(cleanCSS({compatibility: "ie8"}))
	.pipe(rename("vendors.min.css"))
	//.pipe(sourcemaps.write()) // Add the map to modified source.
	.pipe(gulp.dest(output.stylesheets));
});


gulp.task("build-js", function() {
	return gulp.src(js_input.javascript)
  .pipe(sourcemaps.init())
  .pipe(concat("script.js"))
  .pipe(gulp.dest(output.javascript))
  .pipe(rename("scripts.min.js"))
  .pipe(uglify())
  //.pipe(sourcemaps.write())
  .pipe(gulp.dest(output.javascript));
});

gulp.task("vendor-js", function() {
	return gulp.src([
		js_input.modernizr, 
		js_input.jquery, 
		js_input.flexslider, 
		js_input.slick_carousel, 
		js_input.magnific_popup
	])
	.pipe(concat("vendors.js"))
  .pipe(rename("vendors.min.js"))
  .pipe(uglify())
	.pipe(gulp.dest(output.javascript));
});


gulp.task("watch", function(){
  gulp.watch(css_input.stylesheet,["build-css"]);
  gulp.watch(js_input.javascript,["build-js"]);
});

gulp.task("default", ["vendor-css", "vendor-js", "build-css","build-js"]);
